public class SortGrid {
    public static void sortA(Grid g) {

        for (int i = 0; i < g.size()*g.size() - 1; ++i) {
            for (int j = 0; j < (g.size()*g.size()) - 1 - i; ++j) {

                if (g.getIntVal(j / g.size(),j % g.size()) > g.getIntVal((j + 1) / g.size(),(j + 1) % g.size())) {
                    g.swap((j + 1) / g.size(),(j + 1) % g.size(),j / g.size(),j % g.size());
                  
                }
            }
        }
     

    }

    public static void sortB(Grid g) {
        for (int i = 0; i < g.size()*g.size() - 1; ++i) {
            for (int j = 0; j < (g.size()*g.size()) - 1 - i; ++j) {

                if (g.getIntVal(j / g.size(),j % g.size()) > g.getIntVal((j + 1) / g.size(),(j + 1) % g.size())) {
                    g.swap((j + 1) / g.size(),(j + 1) % g.size(),j / g.size(),j % g.size());
                  
                }
            }
        }
    }
}
