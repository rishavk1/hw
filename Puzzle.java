import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
public class Puzzle {
    private Grid grid;
    HashSet<Loc> visitedDFS = new HashSet<Loc>();
    Deque<Loc> path = new Deque<Loc>();
    //constructor--may be updated as necessary
    public Puzzle(Grid grid) {
	this.grid = grid;
    path = new Deque<Loc>();
    }

    //required method to be implemented
    //find "word" in the grid starting at location (r, c)
    //return the path as a String
    public String find(String word, int r, int c) throws EmptyDequeException {
	//TO BE IMPLEMENTED
        bfs(word,r,c);
        String resultPath = "";
        int o = path.size();
        
        for(int i=0;i<o-1;i++){
            Loc x = path.getFirst();
            resultPath+= x.toString();
        }
        path = new Deque<Loc>();
        visitedDFS = new HashSet<Loc>();
        return resultPath;
    }
    public Deque<Loc> bfs(String word, int r, int c) throws EmptyDequeException{
        Deque<Loc> queue = new Deque<Loc>();
        Deque<Loc> visited = new Deque<Loc>();
        queue.addToFront(new Loc(r,c,null));
        int[][] dir = {{0,1},{1,0},{0,-1},{-1,0}};

        while(!queue.isEmpty()){
           
           Loc curr = queue.getLast();
           visited.addToFront(curr);
           if(curr.row>=grid.size() || curr.col>=grid.size() || curr.row<0 || curr.col<0 )
                continue;
          
           if(grid.getVal(curr.row, curr.col).charAt(0) == word.charAt(0)){

                if(dfs(word,curr.row,curr.col,0))
                {
                    path.addToFront(new Loc(curr.row,curr.col,grid.getVal(curr.row, curr.col)));
                    return path;
                }
           }
           for(int[] i:dir){
            if(!queue.contains(new Loc(i[0]+curr.row,i[1]+curr.col,null)) && !visited.contains(new Loc(i[0]+curr.row,i[1]+curr.col,null))){
                queue.addToFront(new Loc(i[0]+curr.row,i[1]+curr.col,null));
            }

           }
        }
      
        return null;
    }



    public boolean dfs(String word, int r, int c, int charIndex) throws EmptyDequeException{
        
        if(charIndex == word.length()){

            return true;
        }
        if(r<0 || c<0 || r == grid.size() || c == grid.size()){
            return false;
        }
        if(visitedDFS.contains(new Loc(r,c,null))){

            return false;
        }

        if(word.charAt(charIndex) == grid.getVal(r, c).charAt(0)){
            visitedDFS.add(new Loc(r,c,null));

            if(dfs(word,r-1,c,charIndex+1)){
                path.addToFront(new Loc(r-1,c,grid.getVal(r-1,c)));

                return true;
            }
            if(dfs(word,r,c+1,charIndex+1)){
                path.addToFront(new Loc(r,c+1,grid.getVal(r,c+1)));

                return true;
            }
            if(dfs(word,r+1,c,charIndex+1)){
                path.addToFront(new Loc(r+1,c,grid.getVal(r+1, c)));

                return true;
            }
            if(dfs(word,r,c-1,charIndex+1)){
                path.addToFront(new Loc(r,c-1,grid.getVal(r, c-1)));

                return true;
            }
            //back tracking
            visitedDFS.remove(new Loc(r,c,null));
        }
        
        
        return false;
    }
}
    

