public class ArrList {
    Array arr;
    int start_index;
    int end_index;
    int size;
    int cap;
    public ArrList(){
        arr = new Array(10);
        cap = 10;
        start_index = 0;
        end_index = arr.length()-1;
        size = 0;
    }
    public ArrList(int cap){
        arr = new Array(cap);
        this.cap = cap;
        start_index = 0;
        end_index = arr.length()-1;
        size=0;
    }
    void addLast(int num){
        if(size == cap){
            arr.resize(cap*2, 0, cap*2);
            end_index = end_index+cap;
            cap = cap*2;
        }
        
        arr.setVal(start_index++, num);
        size++;
        
    }
    void addFirst(int num){
        if(size == cap){
            arr.resize(cap*2, 0, cap*2);
            end_index = end_index+cap;
            cap = cap*2;
           
        }
        arr.setVal((end_index--),num);
        size++;
    }
    int get(int i){
     if(cap-end_index>=i+2){
        return arr.getVal(end_index+i+1);
     }
     else{
        int x = i-cap+end_index+1;
        return arr.getVal(x);
     }
    }

    int indexOf(int num){
        for(int i=0;i<size;i++){
            if(num == get(i)){
                return i;
            }
        }
        return -1;
    }
    boolean contains(int num){
        return indexOf(num)==-1?false:true;
    }
    boolean isEmpty(){
        return size==0?true:false;
    }
    int lastIndexOf(int num){
        for(int i=size-1;i>=0;i--){
            if(num == get(i)){
                return i;
            }
        }
        return -1;
    }
    int removeFirst() throws EmptyListException{
        if(size == 0){
            throw new EmptyListException();
        }
        int x = arr.getVal(end_index+1);
        arr.setVal(end_index++,0);
        size--;
     
        return x;
    }
    int removeLast() throws EmptyListException{
        if(size == 0){
            throw new EmptyListException();
        }
        int x = arr.getVal(start_index-1);
        arr.setVal(--start_index,0);
        size--;
        return x;
    }
    int removeByIndex(int i) throws EmptyListException{
        if(size == 0){
            throw new EmptyListException();
        }
        int x;
        if(cap-end_index>=i+2){
            x = arr.getVal(end_index+i+1);
            arr.setVal(end_index+i+1, 0);
            for(int j=end_index+i;j>end_index;j--){
                arr.swap(j+1,j);
            }
           
        }
        else{
            x = arr.getVal(i-cap+end_index+1);
            arr.setVal(i-cap+end_index+1, 0);
            for(int j=i-cap+end_index+1;j<start_index-1;j++){
                arr.swap(j+1,j);
            }
        }
        size--;
        return x;
    }
    boolean removeByValue(int num) throws EmptyListException{
        int i = indexOf(num);
        if(i==-1){
            return false;
        }
        removeByIndex(i);
        size--;
        return true;
    }
    int set(int index,int num){
        int x = get(index);
        if(cap-end_index>=index+2){
                arr.setVal(end_index+index+1,num);
            }
            else{
               
                arr.setVal(index-cap+end_index+1,num);
            }
        return x;
    }
    int size(){
        return size;
    }

    public int getAccessCount() {
        return arr.getAccessCount();
    }
    public void resetAccessCount() {
        arr.resetAccessCount();
    }

}
