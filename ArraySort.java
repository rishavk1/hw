public class ArraySort {
    public static void sort(Array array, int num, int d){
        if(num == 1){
            Queue q = new Queue();
            for(int i=array.length()-1;i>=0;i--){
                q.enqueue(array.getVal(i));
            }
            for(int i=0;i<array.length();i++){
                array.setVal(i,(int)q.dequeue());
            }
        }
        if(num == 2){
            int n = array.length();
            for(int i=1;i<n;i++){
                int key = array.getVal(i);
                int j = i-1;
                while(j>=0 && array.getVal(j)>key){
                    array.setVal(j+1,array.getVal(j));
                    j = j-1;
                }
                array.setVal(j+1,key);
            }
        }
        if(num == 3){
            int n = array.length();
            for(int i=1;i<n;i++){
                int key = array.getVal(i);
                int j = i-1;
                while(j>=0 && array.getVal(j)>key){
                    array.setVal(j+1,array.getVal(j));
                    j = j-1;
                }
                array.setVal(j+1,key);
            }
        }
        if(num == 4){
            int n = array.length();
            for(int i=1;i<n;i++){
                int key = array.getVal(i);
                int j = i-1;
                while(j>=0 && array.getVal(j)>key){
                    array.setVal(j+1,array.getVal(j));
                    j = j-1;
                }
                array.setVal(j+1,key);
            }
        }
        if(num == 5){
            int n = array.length();
            for (int i = 0; i < n - 1; i++)
                for (int j = 0; j < n - i - 1; j++)
                    if (array.getVal(j) > array.getVal(j + 1)) {
                        array.swap(j,j+1);
                    }
        }
        if(num == 6){
            int n = array.length();
            array.initExtra(n);
            mergeSort(array,0,n-1);
        }
        if(num == 7){
            int min = array.getVal(0);
            for(int i=1;i<array.length();i++){
                if(array.getVal(i)<min){
                    min = array.getVal(i);
                }
            }
            for(int i=0;i<array.length();i++){
                array.setVal(i,array.getVal(i)-min);
            }
            int max = array.getVal(0);
           
            for(int i=1;i<array.length();i++){

                if(array.getVal(i)>max){
                    max = array.getVal(i);
                }
            }
            System.out.println(max);
            for(int exp = 1; max/exp>0;exp*=10){
                array.initExtra(array.length());
                int i;
                int count[] = new int[10];
                for(int k=0;k<count.length;k++){
                    count[k] = 0;
                }
                for(i=0;i<array.length();i++){
                    count[(array.getVal(i)/exp)%10]++;
                }
                for(i=1; i<10;i++){
                    count[i]+=count[i-1];
                }
                for(i = array.length()-1; i>=0;i--){
                    array.setExtraVal(count[(array.getVal(i)/exp) % 10] - 1,array.getVal(i)); //
                    count[(array.getVal(i)/exp)%10]--;
                }
                for(i=0;i<array.length();i++){
                    array.setVal(i,array.getExtraVal(i));
                }
            }
            for(int i=0;i<array.length();i++){
                array.setVal(i,array.getVal(i)+min);
            }
        }
    }

    static void mergeSort(Array array, int i, int j){
        if(j-i<1)
            return;
        int m = (j+i)/2;
        mergeSort(array,i,m);
        mergeSort(array,m+1,j);
        merge(array, i, m, m+1, j);
    }
    static void merge(Array array, int a, int b, int x, int y){
        int k = a;
        int c = a;
        while(a<=b || x<=y){
            if(a>b){
                array.setExtraVal(c,array.getVal(x));
                c++;
                x++;
            }
            else if(x>y){
                array.setExtraVal(c, array.getVal(a));
                c++;
                a++;
            }
            else if(array.getVal(a)<=array.getVal(x)){
                array.setExtraVal(c,array.getVal(a));
                c++;
                a++;
            }
            else{
                array.setExtraVal(c, array.getVal(x));
                c++;
                x++;
            }
        }
        for(int i=k;i<=y;i++){
            array.setVal(i,array.getExtraVal(i));
        }
    }


}
