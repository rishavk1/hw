.data

FIBO_STR: .asciiz "Fibonacci Numbers:\n"
FIBO_0 : .asciiz "  0: 1\n"
FIBO_1: .asciiz "  1: 1\n"
ASCEND: .asciiz "Run Check: ASCENDING"
DESCEND: .asciiz "Run Check: DESCENDING"
NEITHER: .asciiz "Run Check: NEITHER"
WORD_COUNT: .asciiz "Word Count: "
STRING_SWAP: .asciiz "String successfully swapped!\n"
prev: .word 1
beforeThat: .word 1
space: .asciiz " "
nextLine: .asciiz "\n"
colon: .asciiz ":"
plus: .byte '+'
minus: .byte '-'
bar: .byte '|'
blankSpace: .byte ' '
nullChar: .byte '\0'
nextLine_char: .byte '\n'

.text 

.globl studentMain
studentMain:
    addiu $sp, $sp, -24 # allocate stack space -- default of 24 here
    sw    $fp, 0($sp) # save caller’s frame pointer
    sw    $ra, 4($sp) # save return address
    addiu $fp, $sp, 20 # setup main’s frame pointer
	
	la $s0, fib    # load address of fib into s0
	lw $s0, 0($s0) # load value of fib into s0
	
	la $s1, square # load address of square into s1
	lw $s1, 0($s1) # load value of square into s1
	
	la $s2, runCheck # load address of runCheck into s2
	lw $s2, 0($s2) # load value of runCheck into s2
	
	la $s3, countWords # load address of countWords into s3
	lw $s3, 0($s3) # load value of runCheck into s3
	
	la $s4, revString # load address of revString into $s4
	lw $s4, 0($s4) # load value of revString into $s4
	
	#Task 1: Fibonacci
	# if (fib != 0)
	        # if (fib != 0)
        # {
        # printf("Fibonacci Numbers:\n");
        # printf(" 0: 1\n");
        # printf(" 1: 1\n");
        # int prev = 1, beforeThat = 1;
        # int n = 2;
        # while (n <= fib)
        # {
        # /* REMINDER: C’s printf() function uses "format specifiers," like
        # * %d, as the markers for where to drop in the values
        # * that follow. In the code below, the first %d should
        # * be replaced by the value of the variable n, and the
        # * second by cur.
        # */
        # int cur = prev+beforeThat;
        # printf(" %d: %d\n", n, cur);
        # n++;
        # beforeThat = prev;
        # prev = cur;
        # }
        # printf("\n");
        # }
        # Registers: 
        # s0: fib
        # t1: prev
        # t2: beforeThat
        # t3: n
        # t4: cur
        
	beq $s0, $zero, SQUARE
	
	addi $v0, $zero,4  #print str
	la $a0, FIBO_STR   #print "Fibonacci numbers:\n"
	syscall 
	
	addi $v0, $zero,4  #print str
	la $a0, FIBO_0	   #print "" 0: 1\n"
	syscall
	
	addi $v0, $zero,4  #print str
	la $a0, FIBO_1	   #print "" 0: 1\n"
	syscall
	
	addi $t1,$zero,1  #prev = 1
	
	addi  $t2,$zero,1 #beforeThat=1
	
	addi $t3,$zero,2 #n=3
	
	addi $t8,$s0,1
FIBO_LOOP:
	slt $t4, $t3, $t8 	   #if n<fib loop runs
	beq $t4,$zero,END_FIBO_LOOP# if n>fib j to end of loop

	
	add $t4, $t1, $t2  #int cur = prev+beforeThat

	
	addi $v0, $zero,4 #print str
	la $a0, space     #print " " 
	syscall 
	
	addi $v0, $zero,4 #print str
	la $a0, space     #print " " 
	syscall 
	
	addi $v0, $zero,1 #print int
	add $a0, $zero,$t3	  #print n
	syscall
	
	addi $v0, $zero,4 #print str
	la $a0, colon     #print ":"
	syscall
	
	addi $v0, $zero,4 #print str
	la $a0, space	  #print " "
	syscall
	
	addi $v0, $zero, 1#print int
	add $a0,$zero ,$t4	  #print cur
	syscall
	
	addi $v0, $zero, 4#print str
	la $a0, nextLine  #print "\n"
	syscall
	
	addi $t3, $t3, 1  #n++
	
	add $t2, $zero, $t1 #beforeThat = prev
	
	add $t1, $zero, $t4 #prev = cur
	
	j FIBO_LOOP
END_FIBO_LOOP: 
	addi $v0, $zero,4 #print str
	la $a0, nextLine
	syscall
	
	# SQUARE:
	# if (square != 0)
        # {

        # for (int row=0; row < square_size; row++)
        # {
        # char lr, mid;
        # if (row == 0 || row == square_size-1)
        # {
        # lr = ’+’;
        # mid = ’-’;
        # }
        # else
        # {
        # lr = ’|’;
        # mid = square_fill;
        # }
        # /* The %c format specifier, in printf(), prints a single
        # * character. MIPS syscall 11 will be useful here.
        # */
        # printf("%c", lr);
        # for (int i=1; i<square_size-1; i++)
        # printf("%c", mid);
        # printf("%c\n", lr);
        # }
        # printf("\n");
        # }
        # Registers:
        # $s1: square
        # $t0: row
        # $t1: lr
        #
        # $t3: square_size
        # $t4: square_fill
        # $s5: lr
        # $s6: mid
        # $s7: i
	
SQUARE:

	beq $s1, $zero, RUN_CHECK #if square == 0 jump to RUN_CHECK
	
	la $t3, square_size
	lw $t3, 0($t3) 		  #load square_size into $t3
	
	
	add $t0, $zero, $zero	  #row = 0
	LOOP_SQUARE_START:
            slt $t8, $t0, $t3
            beq $t8, $zero, LOOP_SQUARE_END
        

	addi $t6, $zero, 1
	sub $t6, $t3, $t6	  #$t6 = square_size - 1
	
  #$t5 = 1 if row==0, $t7 = 1 if row == square_size-1, so or on $t7,$t5 should give 1
				  # if our condition is true
	beq $t0,$zero,FALSE
	beq $t0,$t6, FALSE 
		            	
           	la $t7, bar
           	lb $s5, 0($t7)	#$s5 = '|' -> lr = '|'
           	
           	la $t4, square_fill
           	lb $s6, 0($t4)
            	j TRUE
            	FALSE:
            	
            	la $t7, plus	#loads address of plus into $t7
            	lb $s5, 0($t7) 	#lr which $s5 = '+'
            	
            	la $t7, minus   #loads address of minus into $t7
            	lb $s6, 0($t7)  #mid which is $s6 = '-'
            	

           	TRUE:
           
        
        addi $v0,$zero, 11 #print char
        add $a0,$zero ,$s5	  #print lr
        syscall
        	
        	addi $s7, $zero, 1	#i=1 intialization before the loop
        	INNER_LOOP_START:
        		addi $t7, $t3,-1 #$t7=square_size-1
        		slt $t8, $s7, $t7#if i<square_size-1 $t8=1
        		beq $t8,$zero,INNER_LOOP_END
        		addi $v0, $zero,11 #print char
        		add $a0,$zero,$s6  #print mid
        		syscall
        		addi $s7,$s7,1 #i++
        		j INNER_LOOP_START
        	INNER_LOOP_END:
        	addi $v0,$zero, 11 #print char
        	add $a0, $zero,$s5	   #print lr
        	syscall
        	
        	addi $v0,$zero,4   #print str
        	la $a0, nextLine   #print "\n"
        	syscall
        
        addi $t0,$t0,1	#row++
	j LOOP_SQUARE_START
	LOOP_SQUARE_END:
	
	addi $v0,$zero,4   #print str
        la $a0, nextLine   #print "\n"
        syscall

#RUN_CHECK
# int intArray[],intArray_len, a = 1, d = 1, i=0;
#
# while (i < intArray_len - 1) {
#     if (intArray[i] < intArray[i+1])
#       d = 0;
#     else if (intArray[i] > intArray[i+1])
#       a = 0;
#     i++;
#   }
# if (a == 1)
#     printf("Run Check: ASCENDING.\n");
#   else if (d == 1)
#     printf("Run Check: ASCENDING\n");
#   else
#     printf("Run Check: NEITHER\n");	
#
# Registers: intArray_len= $t0
#	     a= $t1
#	     d= $t2
#	     i= $t3
#	     intArray = $t4
#	     
RUN_CHECK:
	addi $t0,$zero,1
	bne $s2,$t0,COUNT_WORDS
	
	la $t0, intArray_len
	lw $t0, 0($t0)
	
	#$t0!=0 || $t!=1 jump to END_INTIAL_CHECK_RUN
	bne $t0,$zero, END_ZERO_ONE_CHECK
	addi $v0,$zero,4
	la $a0, ASCEND	#print "Run Check: ASCENDING"
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	
	addi $v0,$zero,4
	la $a0, DESCEND #print "Run Check: DESCENDING"
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	
	j COUNT_WORDS
	
	addi $t1,$zero,1
	bne $t0,$t1,END_ZERO_ONE_CHECK
	addi $v0,$zero,4
	la $a0, ASCEND	#print "Run Check: ASCENDING"
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	
	addi $v0,$zero,4
	la $a0, DESCEND #print "Run Check: DESCENDING"
	syscall
	
		
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	
	j COUNT_WORDS
	END_ZERO_ONE_CHECK:
	
	
	addi $t1, $zero,1	#a=1
	
	addi $t2, $zero,1 	#d=1
	
	add $t3, $zero,$zero   #i=0
	
	la $t4, intArray
	
	addi $t0,$t0,-1
	
	LOOP_BEGIN_RUN_CHECK:
	slt $t5,$t3,$t0
	beq $t5,$zero,END_LOOP_RUN_CHECK
	
	lw $t6,0($t4)
	addi $t4,$t4,4
	lw $t7,0($t4)
	
	beq $t6,$t7, LOOP_BEGIN_RUN_CHECK
	
	slt $t8,$t6,$t7		#$t8 = 0 => arr[i]>=arr[i+1] => not ascending
	slt $t9,$t7,$t6		#$t9 = 0 => arr[i+1]>=arr[i] => not descending
	
	bne $t8,$zero,ASCEND_END
	add $t1,$zero,$zero
	ASCEND_END:
	
	bne $t9, $zero, DESCEND_END
	add $t2,$zero,$zero
	DESCEND_END:
	addi $t3,$t3,1
	j LOOP_BEGIN_RUN_CHECK
	END_LOOP_RUN_CHECK:
	
	beq $t1,$zero,JUMP_CHECK_1
	addi $v0, $zero, 4
	la $a0, ASCEND	#print "Run Check: ASCENDING"
	syscall
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	JUMP_CHECK_1:
	beq $t2,$zero,JUMP_CHECK_2
	addi $v0,$zero,4
	la $a0, DESCEND #print "Run Check: DESCENDING"
	syscall
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	j END
	JUMP_CHECK_2:
	addi $v0,$zero,4
	la $a0,NEITHER	#print "Run Check: NEITHER"
	syscall
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	END:
	addi $v0,$zero,4
	la $a0,nextLine	#print empty line
	syscall
	
#COUNT_WORDS
# int w = 0, i = 0;

# 	for(i = 0; str[i] != '\0'; i++){
# 		if(str[i] != ' ' && str[i] != '\n'){
# 			w++;
# 			while(str[i] != ' ' && str[i] != '\n')
# 				i++;
# 		}
# 	}

# printf("Word Count: %d\n", w);
# REGISTERS: 
#	     $t0 = str address
#	     $t1 = str at i address
#	     $t5 = w
#	     $t6 = i 
	
COUNT_WORDS:
	addi $t0,$zero,1
	bne $s3,$t0, REV_STRING
	
	la $t0, str
	add $t5, $zero,$zero # w=0
	add $t6, $zero,$zero # i=0 
	LOOP_START_COUNT_WORDS:
	lb $t1,0($t0)
	la $t2, nullChar
	lb $t2, 0($t2)			   #load '\0'
	beq $t1, $t2, LOOP_END_COUNT_WORDS #if str[i] == '\0' jump to end of LOOP
		la $t2, blankSpace
		lb $t2, 0($t2)
		beq $t1, $t2, END_WHILE_LOOP
		la $t2, nextLine_char
		lb $t2, 0($t2)
		beq $t1, $t2, END_WHILE_LOOP #if(str[i] == ' ' || str[i] == '\n') then j to while loop
		addi $t5,$t5, 1			#w++
			START_WHILE_LOOP:
				la $t2, blankSpace
				lb $t2, 0($t2)
				beq $t1, $t2, END_WHILE_LOOP
				la $t2, nextLine_char
				lb $t2, 0($t2)
				beq $t1, $t2, END_WHILE_LOOP #if(str[i] == ' ' || str[i] == '\n') then j to end of while loop
				addi $t6, $t6, 1 #i++
				addi $t0,$t0, 1
				lb $t1,0($t0)	#move str[i] to str[i+1]
				la $t2, nullChar
				lb $t2, 0($t2)			   #load '\0'
				beq $t1, $t2, LOOP_END_COUNT_WORDS #if str[i] == '\0' jump to end of LOOP
				j START_WHILE_LOOP
			END_WHILE_LOOP:
		

	addi $t6,$t6, 1 	#i++	
	addi $t0,$t0,1		#move str[i] to str[i+1]
	j LOOP_START_COUNT_WORDS
	LOOP_END_COUNT_WORDS:
	

	
	addi $v0,$zero,1
	add $a0,$zero, $t5 #print w
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	   #print "\n"
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	   #print "\n"
	syscall
	
#revString
# if (revString != 0)
# {
# int head = 0;
# int tail = 0;
# // advance the tail until we find the correct location
# //
# // NOTE: The code ’\0’ is the "null character" - that is, it’s the
# // character with ASCII code 0x00. Thus, the loop below is
# // hunting for the null terminator, which marks the end of
# // the string.
# while (str[tail] != ’\0’)
# tail++;
# tail--;
# // when we get here, tail gives the index of the last non-NULL
# // character in the string. If the string is empty, it actually
# // would be -1, which is weird but OK.
# while (head < tail)
# {
# ... swap the two characters str[head] and str[tail] ...
# head++;
# tail--;
# }
# printf("String successfully swapped!\n");
# printf("\n");
# }
#Registers: head = $t1
#	    tail = $t2
#	    str address = $t3
#	    str[i] = $t4
REV_STRING:
	addi $t0, $zero, 1
	bne $s4,$t0, EPILOGUE
	
	
	add $t1, $zero,$zero #$t1=0: head = 0
	add $t2, $zero,$zero #$t2=0: tail = 0
	
	la $t3,str	     #load str address into $t3
	lb $t4, 0($t3)	     #load str[i]
	
	LOOP_FIND_NULL_START:
		la $t5, nullChar
		lb $t5, 0($t5)
		beq $t4,$t5, LOOP_FIND_NULL_END #if str[tail] == '\0' jump to LOOP_FIND_NULL_END
		addi $t2,$t2,1	#tail++
		addi $t3,$t3,1	#move address to str[tail+1]
		lb $t4, 0($t3)	#load str[tail+1]
		j LOOP_FIND_NULL_START
	LOOP_FIND_NULL_END:
	addi $t9,$zero,1
	sub $t2,$t2,$t9	#tail-- to move back from null character
	
	addi $s6, $zero, 1
	
	START_HEAD_TAIL:
	slt $t9, $t1,$t2 
	beq $t9, $zero, END_HEAD_TAIL
	
	la $t3,str	     #load str address into $
	add $t6,$t3,$t1      #$t6 = str address + offset of head
	add $t7,$t3,$t2     #$t6 = str address + offset of tail
	
	lb $t8,0($t6)	   #load str[head] into $t8
	lb $t9, 0($t7)	   #load str[tail] into $t9
	
	sb $t8, 0($t7)	  #save $t9 into 
	sb $t9, 0($t6)
	
	addi $t1,$t1,1	#head++
	sub $t2,$t2,$s6	#tail--
	j START_HEAD_TAIL
	END_HEAD_TAIL:
	
	addi $v0,$zero,4
	la $a0, STRING_SWAP #print "String successfully swapped"
	syscall
	
	addi $v0,$zero,4
	la $a0,nextLine	   #print "\n"
	syscall
	
	
EPILOGUE:
	lw    $ra, 4($sp) # get return address from stack
	lw    $fp, 0($sp) # restore the caller’s frame pointer
	addiu $sp, $sp, 24 # restore the caller’s stack pointer
	jr    $ra # return to caller’s code
