public class Part1 {
    public static int maxProduct(Array a, int m){
        //Implementing a sliding window solution
        //first we take care of base cases
        if(a.length() == 0){
            return 1;
        }
        if(a.length() == 1){
            return a.getVal(0);
        }
        if(a.length()<m){
            int product = 1;
            for(int i=0;i<a.length();i++){
                product = product*a.getVal(i);
            }
            return product;
        }
        int product = 1;
        
        // preparing a m-sized window and saving the product in a variable
        for(int i=0;i<m;i++){
            product *= a.getVal(i);
        }
        int maxProduct = Integer.MIN_VALUE;
        for(int i=m;i<a.length();i++){
            //changing max product if its less than product
            if(maxProduct<product){
                maxProduct = product;
            }
            //the following lines are for shifting the window accordingly
            product = product/a.getVal(i-m);
            product = product*a.getVal(i);
            
        }
        //checking the last variable if it computes to max product
        if(maxProduct<product){
            maxProduct = product;
        }
        return maxProduct;
    }
}
